const bcrypt = require('bcryptjs');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const Post = require('../models/post')
const {clearImage} = require('../util/file');

module.exports = {
    createUser : async function({userInput}, req){

        const errors = [];
        if (!validator.isEmail(userInput.email)){
            errors.push({message : 'Email is invalid'})
        }
        if (validator.isEmpty(userInput.password) ||
            !validator.isLength(userInput.password, {min : 5})){
                errors.push({message : 'password invalid to short'})

        }
        if(errors.length > 0) {
            const error= new Error('invalid input');
            error.data = errors;
            error.code = 422;
            throw error;
        }

        //sinon on utilise return User.findOne : return important
        const existingUser = await User.findOne({email : userInput.email}) 
        if (existingUser){
            const error = new Error('User exists already');
            throw error;
        }  
        const hashPwd = await bcrypt.hash(userInput.password,12);
        const user = new User({
            email : userInput.email,
            name: userInput.name,
            password: hashPwd
        })    
        const createdUser = await user.save();
        return {...createdUser._doc, _id: createdUser._id.toString()}
    },

    login : async function({email, password}) {
        const user = await User.findOne({email : email});
        if (!user){
            const error = new Error('User not found');
            error.code = 401;
            throw  error;
        }
        const isEqual = bcrypt.compare(password,user.password);
        if (!isEqual){
            const error = new Error('password incorrect');
            error.code = 401;
            throw error
        }
        const token = jwt.sign(
            {
                userId : user._id.toString(),
                email : user.email
            },
            'somesupersecret',
            {expiresIn : '1h'}
        )

        return {token : token , userId : user._id.toString()}
    }, 

    createPost : async function({postInput}, req){
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }
        const errors = [];
        if (validator.isEmpty(postInput.title) ||
            !validator.isLength(postInput.title, {min : 5})){
                errors.push({message : 'title invalid'})

        }
        if (validator.isEmpty(postInput.content) ||
            !validator.isLength(postInput.content, {min : 5})){
                errors.push({message : 'content invalid'})

        }
        if(errors.length > 0) {
            const error= new Error('invalid input');
            error.data = errors;
            error.code = 422;
            throw error;
        }

        const user = await User.findById(req.userId);
        if (!user){
            const error= new Error('invalid user');
            error.data = errors;
            error.code = 401;
            throw error;
        }

        const post =  new Post({
            title : postInput.title,
            content : postInput.content,
            imageUrl : postInput.imageUrl,
            creator : user
        });
        const createdPost = await post.save();
        user.posts.push(createdPost);
        await user.save();
        return {
          ...createdPost._doc,
          _id: createdPost._id.toString(),
          createdAt : createdPost.createdAt.toISOString(),
          updatedAt : createdPost.updatedAt.toISOString(),
        };
    },

    posts : async function({page},req) {
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }

        const currentPage = page ||1;
        const perPage = 2;
        let totalPosts = await Post.find()
                            .countDocuments()
        const posts = await Post.find()
                    .populate('creator')
                    .sort({createdAt : -1})
                    .skip((currentPage -1) * perPage)
                    .limit(perPage);

        return {
          posts : posts.map(p => {
              return {
                  ...p._doc,
                  _id : p._id.toString(),
                  createdAt : p.createdAt.toISOString(),
                  updatedAt : p.updatedAt.toISOString(),
              }
          }),
          totalPosts : totalPosts,
        };

    
    },
    post : async function({id} , req) {
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }

        const post = await Post.findById(id).populate('creator')

        if (!post){
            const error = new Error('could not find post');
            error.statusCode = 404;
            throw error //send to catch
        }

        return {
            ...post._doc,
            _id : post._id.toString(),
            createdAt : post.createdAt.toISOString(),
            updatedAt : post.updatedAt.toISOString(),

        }

    },
    updatePost : async function({id, postInput}, req){
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }
        const post = await Post.findById(id).populate('creator')
        if (!post){
            const error = new Error('could not find post');
            error.statusCode = 404;
            throw error //send to catch
        }
        if (post.creator._id.toString() !==req.userId.toString()){
            const error = new Error('not authenticated !!!!');
            error.code = 403;
            throw error;
        }
        //validation ****
        const errors = [];
        if (validator.isEmpty(postInput.title) ||
            !validator.isLength(postInput.title, {min : 5})){
                errors.push({message : 'title invalid'})

        }
        if (validator.isEmpty(postInput.content) ||
            !validator.isLength(postInput.content, {min : 5})){
                errors.push({message : 'content invalid'})

        }
        if(errors.length > 0) {
            const error= new Error('invalid input');
            error.data = errors;
            error.code = 422;
            throw error;
        }
        //******
        post.title = postInput.title;
        post.content = postInput.content;
        if (postInput.imageUrl !=='undefined'){
            post.imageUrl = postInput.imageUrl.replace("\\" ,"/");
        }
        const updatedPost = await post.save();
        return {
            ...updatedPost,
            _id: updatedPost._id.toString(),
            createdAt : updatedPost.createdAt.toISOString(),
            updatedAt : updatedPost.updatedAt.toISOString(),
        }

    },
    deletePost : async function({id}, req){
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }

        const post = await Post.findById(id)
        if (!post){
            const error = new Error('could not find post');
            error.statusCode = 404;
            throw error //send to catch
        }
        if(post.creator.toString() !== req.userId){
            const error = new Error('Not Authorized');
            error.statusCode = 403;
            throw error //send to catch
        }

        clearImage(post.imageUrl);
        await Post.findByIdAndRemove(id)
        const user = await User.findById(req.userId)
        user.posts.pull(id);
        await user.save();  

        return true;

    },
    user : async function(args, req){
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }

        const user = await User.findById(req.userId)
        if (!user){
            const error = new Error('User not found -user');
            error.statusCode = 403;
            throw error //send to catch
        }
        return {
            ...user._doc,
            _id : user._id.toString()
        };
    },
    updateStatus : async function({status}, req){
        if (!req.isAuth){
            const error = new Error('not authenticated !!!!');
            error.code = 401;
            throw error;
        }
        const user = await User.findById(req.userId);

        if (!user){
            const error = new Error('User not found -updatestatus');
            error.statusCode = 403;
            throw error //send to catch
        }
        user.status = status;
        await user.save()

        return {
            ...user._doc,
            _id : user._id.toString()
        };
    }
}